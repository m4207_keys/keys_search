#include <LGSM.h>                                                 // Include Linkit ONE GSM library
String messageConfirm = "What do you want to send?";              // Asks user for input and stores it in the messageConfirm variable
String message;                                                   // Variable to store message to be sent
String confirmSend = "Do you want to send the SMS (Yes or No)?";  // Stores confirmation for the SMS to be sent
String confirmReply;                                              // Variable to store users input to confirm sending SMS

void setup() {
  
  // put your setup code here, to run once:

  Serial.begin(115200);                 // Initializes serial port at 115200 bauds

  Serial.println("...Pret a envoyer les infos..."); // Start the Send SMS program
  
  while (!LSMS.ready())                 // Wait for the sim to initialize
  {
    delay(1000);                        // Wait for a second and then try again
  }
  
  Serial.println("Sim initialized...");    // When SIM is started, print "Sim initialized" in the serial port

  LSMS.beginSMS("0692062741");          // Saves the number where user wants to send SMS. To be changed before uploading sketch

}

void loop() { 
  
  // Put your main code here, to run repeatedly:
  message = "CECI EST UN TESTE DE MESSAGE";        // Save user input in the message variable

  LSMS.print(message);                  // Prepare message variable to be sent by LSMS
      
    if (LSMS.endSMS())                  // If so, send the SMS
    {
      Serial.println("SMS sent");    // Print "SMS sent" in serial port if sending is successful
    }
    else
    {
      Serial.println("SMS is not sent");// Else print "SMS is not sent"
    }
    delay(5000);
 }
 
