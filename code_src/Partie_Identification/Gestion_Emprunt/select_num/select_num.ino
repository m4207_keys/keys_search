/*
Dans cette partie nous allons emprunté une clée.
Pour ce faire nous allons utiliser le numéro d'étudiants afin d'identifier l'utilisateur.

Prérequis: 
  -Ecran LCD (Afficher les données)   port: I2C
  -Potentiometre (choisir les numéros)  port: A0
  -1 Bouton (valider)       port: D8 

But: 
  -Entrer un numero etudiant de 8 chiffre
  -Envoyer ce numéro via GSM

copyright : Damien ETHEVE
*/

#include <Wire.h>
#include "rgb_lcd.h"

rgb_lcd lcd;                                              //Instance dun LCD

/*Constantes*/
const int potarPin = A0;                                  //attribut la pin du potentiometre
const int buttonPin = 8;                                  //pin du bouton

/*Variable*/
int buttonState = 0;
int potarState = 0;
int potarStateMap = 0;

int numeroEtudiant[7];                                    //Car un numéro etudiant est composé de 8 chiffre
int saveState = 0;                                        //etat de la sauvegarde
String numero = "";                                       //Numero Etudiant Final      
  
void setup() 
{
    /*SETUP LCD*/
    lcd.begin(16, 2);                                     //nombre de ligne et de colonnes

    pinMode(potarPin, INPUT);
    pinMode(buttonPin, INPUT);

    Serial.begin(9600);
    delay(100);
}

void loop() 
{   

    
    lcd.setCursor(0,0);                                    //on se place a ligne 0 et colone 0
    choisirNumero();

    lcd.setCursor(0,1);                                    //on se place a ligne 1 et colone 0
    validerNumero();

    /*ecrire le numéroEtudiant*/
    for(int i = 0 ; i <= 7; i++){                          
      lcd.print(numeroEtudiant[i]);
    }
    //lcd.print(numeroEtudiant[7]);
    
    lcd.blink();
    
}

void choisirNumero(){
  
      /*CHOIX DU NUMERO*/
    potarState = analogRead(potarPin);                    //Lire le potar
    potarStateMap = map(potarState, 0, 1019, 0, 9);       //map la valeur du potar sur 0 a 9
    lcd.print(potarStateMap);                             //puis on eris la valeur du potar
    
}

void validerNumero(){
  
      /*VALIDATION DU NUMERO*/
    buttonState = digitalRead(buttonPin);                 //Lire le bouton
    
    if(buttonState == HIGH){
      lcd.setCursor(0,1);                                 //on se place a ligne 1 et colone 0
      ajouteNum();
      delay(200);
      
    }else{
       potarStateMap = 0;
       //lcd.print("DOWN");
    }
}

void ajouteNum(){
      int val = potarStateMap;
      for(int i = 0  ; i <= 7; i++){                     //On parcour la longueur d'un numéro étudiant
        if(i  == saveState ){                            //Si on es au bon emplacement dans le tableau
          numeroEtudiant[i] = val;                       //Attribue la valeur choisi par le potar au tableau
        }
                               
      }
    saveState++;                                          //incrémente la valeur de l'emplacement ou on se situe dans le tableau


    /*SI TOUT LE NUMERO EST ENTRE*/
    if(saveState > 7){                                    //Si on as fini d'entrer le numéro étudiant
      saveState = 7;                                      //On écris pas plus loin dans le tableau
      validationFinal();                                  //Envois le numero...
    }
}

void validationFinal(){
  lcd.setCursor(13,1); 
  lcd.print("_OK");
  for(int i = 0 ; i <= 7; i++){                          
      numero += numeroEtudiant[i];
  }
  Serial.println(numero);
  while(true){
    lcd.setCursor(0,0); 
    lcd.print("Num:");
    lcd.setCursor(5,0);
    lcd.print(numero);
    lcd.setCursor(0,1);
    lcd.print("                                         ");
    /*...ENVOIS...*/
  }
}

