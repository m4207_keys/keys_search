/* PROJET M4207: Localisation d'une clé du département RT
Réalisé par: Le groupe 3D (ABRADOR Daryl, SINAPAYEL Dany & RAMOUCHE Dorian) 
Ce code a pour but: 
D'initier une connection WiFi avec un AP
De vérifier que l'on es bien connecter à son réseau (optionel pour le moment)
D'optenir l'adresse MAC de la GW à laquelle notre Linkit One est connecté
De réaliser un scan des APs environnant 
D'obtenir le RSSI de notre connection afin d'ajouter une notion de proximité */

// IMPORTATION de librairies
#include <LWiFi.h>
#include <LWiFiClient.h>

#define WIFI_AP_LINK "LinkitOneWifi"            // WiFi AP SSID LINKITONE
#define WIFI_AP_EDU "eduspot"            // WiFi AP SSID EDUSPOT
#define WIFI_AP "RT-WIFI-Guest"            // WiFi AP SSID RT WIFI GUEST
#define WIFI_PASSWORD "wifirtguest"  // WiFi AP RT mdp
#define WIFI_AUTH LWIFI_WPA           // type d'authentification              
char bssid1[18];             // @MAC de la borne WIFI
char bssid2[18];             // @MAC de la borne2 WIFI
//Liste des APs avec leurs adresses MAC et dans quelle Salle ils se trouvent
char *rtwifi[6][3] = {{"92:2A:A8:5A:5C:68","Administration",""},
                       {"92:2A:A8:4A:AD:26","Proj-doc",""},
                       {"92:2A:A8:4A:AD:2E","Reseaux cablages","Info Prog"},
                       {"92:2A:A8:4A:AF:2E","Signaux Systemes",""},
                       {"92:2A:A8:47:CE:A5","TD1","TD2"},
                       {"92:2A:A8:4A:AC:55","TD3","Info Prog"}};

char *eduwifi[7][3] = {{"00:1D:A2:D8:38:B1","Administration"},
                       {"00:07:0E:56:93:80","Proj-doc"},
                       /*{"00:1D:45:26:DD:7A","Reseaux cablages"},
                       {"00:14:A8:39:2C:B6","Signaux Systemes"},*/
                       /*{"00:13:1A:3A:B5:9C","TD1"}, // TD1: 00:1D:A2:D8:3D:E1*/
                       {"00:1D:A2:69:8E:9A","TD2"}, // a voir j'ai relever TD1 et TD2 identique
                       {"00:1D:A2:D8:3D:E1","Genie inf"}, // devrait etre en TD1
                       /*{"00:13:1A:96:7C:D0","Genie inf"},*/
                       {"00:13:1A:96:7D:71","Info prog"},
                       {"00:14:6A:F0:41:31","LPRO"},
                       {"00:14:6A:F0:42:21","LPRO"}};

void setup(){
  Serial.begin(9600);     // Initialisation du port série
  /*while (!Serial) {
    ;                     // attendre qu'il soit ouvert pour la connection 
  }*/


}

void loop() {

   // Connexion a l'AP le plus proche
  LWiFi.begin();        // Initialise le module WiFi

  Serial.print("Connexion au WiFi : \n");
  while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
  {
    delay(1000);
  }

  // Statut de connexion
  StatutWifi();
  //Lance la localisation de la clé
  Localize(rtwifi,eduwifi);
  Serial.println();
  LWiFi.disconnect();

}

// Affichage du statut WiFi
void StatutWifi() {
  //Affiche le nom de l'AP auquelle on c'est connecté 
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  //Afficher le puissance du signal de l'AP
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI): ");
  Serial.print(rssi);
  Serial.println(" dBm");

  //Affiche l'adresse MAC
  if (strcmp(LWiFi.SSID(),"RT-WIFI-Guest")==0){
    uint8_t BSSID[VM_WLAN_WNDRV_MAC_ADDRESS_LEN] = {0};
    LWiFi.BSSID(BSSID);
    Serial.print("BSSID2 is: ");  
    sprintf(bssid2, "%02X:%02X:%02X:%02X:%02X:%02X", BSSID[0], BSSID[1], BSSID[2], BSSID[3], BSSID[4], BSSID[5]);   
    Serial.println(bssid2);
  }
}

//Localisation de la clé
void Localize(char *rtwifi[6][3],char *eduwifi[6][3]){ 
  int i;
  long rssi = LWiFi.RSSI();
  for (i=0;i<6;i++){
    if (strcmp(bssid2,rtwifi[i][0])== 0){
      Serial.println("---------------------------------");
      if(rssi < -58){ //La puissance réçus est inférieur au seuil définit (ici -58), on se trouve donc entre deux salles ayant un AP RT-Wifi
        Serial.print("Je suis en "); 
        Serial.print(rtwifi[i][2]); //On affiche le nom de cette salle
      }
      else{
        Serial.print("Je suis en "); 
       Serial.print(rtwifi[i][1]);//Affiche le nom de la salle
      }      
    }
  }
}

