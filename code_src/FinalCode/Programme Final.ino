/*
 * Auteur: Damien, Ludovic
 * 
 * IDENTIFICATION: Damien
 * LOCALISATION: Ludovic
 * 
 * A0 : potentiomettre
 * D8 : Bouton tactil
 * I2C : LCD
 * Carte sim + anteine
 * 
 * 
 */



#include <LGSM.h>                                         // Include Linkit ONE GSM library
#include <Wire.h>
#include "rgb_lcd.h"

// IMPORTATION de librairies Wifi
#include <LWiFi.h>
#include <LWiFiClient.h>

#define WIFI_AP_LINK "LinkitOneWifi"            // WiFi AP SSID LINKITONE
#define WIFI_AP_EDU "eduspot"            // WiFi AP SSID EDUSPOT
#define WIFI_AP "RT-WIFI-Guest"            // WiFi AP SSID RT WIFI GUEST 
#define WIFI_PASSWORD "wifirtguest"  // WiFi AP RT mdp
#define WIFI_AUTH LWIFI_WPA           // type d'authentification              
char bssid1[18];             // @MAC de la borne WIFI
char bssid2[18];             // @MAC de la borne2 WIFI
//Liste des APs avec leurs adresses MAC et dans quelle Salle ils se trouvent
char *rtwifi[6][3] = {{"92:2A:A8:5A:5C:68","Administration",""},
                       {"92:2A:A8:4A:AD:26","Proj-doc",""},
                       {"92:2A:A8:4A:AD:2E","Reseaux cablages","Info Prog"},
                       {"92:2A:A8:4A:AF:2E","Signaux Systemes",""},
                       {"92:2A:A8:47:CE:A5","TD1","TD2"},
                       {"92:2A:A8:4A:AC:55","TD3","Info Prog"}};
//String message;                                                 

rgb_lcd lcd;                                              //Instance dun LCD

/*Constantes*/
const int potarPin = A0;                                  //attribut la pin du potentiometre
const int buttonPin = 8;                                  //pin du bouton

/*Variable*/
int buttonState = 0;
int potarState = 0;
int potarStateMap = 0;

int numeroEtudiant[7];                                    //Car un numéro etudiant est composé de 8 chiffre
int saveState = 0;                                        //etat de la sauvegarde
String numero = "";                                       //Numero Etudiant Final      

bool isKeep;                                      //Clée prise ou non
  


/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================SETUP=======================================
 * _________________________________________________________________________________________________________________
 *
 */
 
void setup() {


  Serial.begin(9600);
  
  /*SETUP CARTE SIM*/
  /*Serial.println("...Pret a envoyer les infos..."); 
  while (!LSMS.ready())                
  {
    delay(1000);                       
  }
  
  Serial.println("Sim initialisé...");    
  LSMS.beginSMS("0692062741");                              //Numéro de tel de la direction

  */

  /*SETUP SELECNUM*/
  lcd.begin(16, 2);                                         //nombre de ligne et de colonnes
  pinMode(potarPin, INPUT);    
  pinMode(buttonPin, INPUT);

  delay(100);
  
}

/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================PRINCIPAL=======================================
 * _________________________________________________________________________________________________________________
 *
 */

void loop() { 
    
    
    //checkIsKeep();                                            //Regarde si la clée est prise
    /*Serial.print("savestate: ");
    Serial.println(saveState);*/
while(isKeep == false){ //Execute ce qui est dans la boucle dans le numéro complet n'est pas entré
   checkIsKeep();
    if(isKeep == false){
       
       lcd.setCursor(0,0);                                    //on se place a ligne 0 et colone 0
       choisirNumero();

       lcd.setCursor(0,1);                                    //on se place a ligne 1 et colone 0
       validerNumero();

      /*ecrire le numéroEtudiant*/
      for(int i = 0 ; i <= 7; i++){                          
        lcd.print(numeroEtudiant[i]);
      }
      
    }else{
      /*SI TOUT LE NUMERO EST ENTRE*/

      
        envois();                                             //Envois les donéés...
        rendreClee();                                         //gere la remise de la clée
        delay(10000);                                         //Attend 10 sec pour ebvoyer le message
    }
}    
    lcd.blink();

    
 /*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================Lancement Wifi==============================
 * _________________________________________________________________________________________________________________
 *
 */
 // Initialise le module WiFi, apres avoir entrer le numero étudiant
        LWiFi.begin();        

        Serial.print("Connexion au WiFi : \n");
        while (0 == LWiFi.connect(WIFI_AP, LWiFiLoginInfo(WIFI_AUTH, WIFI_PASSWORD)))
          {
            delay(1000);
          }

        // Statut de connexion
        StatutWifi();
        //Lance la localisation de la clé
        Localize(rtwifi);
        Serial.println();
        LWiFi.disconnect();
}

/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================Regarde si la clés est prise==============================
 * _________________________________________________________________________________________________________________
 *
 */

 void checkIsKeep(){
    if(saveState > 7){
      saveState = 7;
      isKeep = true;   
    }else{
      isKeep = false;
    }
 }


/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================ChoisirNuméro==============================
 * _________________________________________________________________________________________________________________
 *
 */
void choisirNumero(){
  
      /*CHOIX DU NUMERO*/
    potarState = analogRead(potarPin);                    //Lire le potar
    potarStateMap = map(potarState, 0, 1019, 0, 9);       //map la valeur du potar sur 0 a 9
    lcd.print(potarStateMap);                             //puis on ecris la valeur du potar
    
}


/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================Valider le numéro=======================================
 *                Si on active le bouton, alors on inject la valeur choisis dans le tableau
 * _________________________________________________________________________________________________________________
 *
 */
void validerNumero(){
  
      /*VALIDATION DU NUMERO*/
    buttonState = digitalRead(buttonPin);                 //Lire le bouton
    
    if(buttonState == HIGH){
      lcd.setCursor(0,1);                                 //on se place a ligne 1 et colone 0
      ajouteNum();
      delay(200);
      
    }else{
       potarStateMap = 0;
       //lcd.print("DOWN");
    }
}

/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================Sauvegarde le numéro=======================================
 *                Rejoind la fonction précédente, permet d'ajouter la valeur dans le tableau et passer a la case suivante
 * _________________________________________________________________________________________________________________
 *
 */

void ajouteNum(){
      int val = potarStateMap;
      for(int i = 0  ; i <= 7; i++){                     //On parcour la longueur d'un numéro étudiant
        if(i  == saveState ){                            //Si on es au bon emplacement dans le tableau
          numeroEtudiant[i] = val;                       //Attribue la valeur choisi par le potar au tableau
        }
                               
      }
    saveState++;                                          //incrémente la valeur de l'emplacement ou on se situe dans le tableau
}


/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================Envoyer les informations=======================================
 *                Envois les donées par GSM 
 * _________________________________________________________________________________________________________________
 *
 */
void envois(){
  /*lcd.setCursor(13,1); 
  lcd.print("_OK");*/
  for(int i = 0 ; i <= 7; i++){                           //Stock le numéro dans une variable afin de mieu la manipuler            
      numero += numeroEtudiant[i];
  }
  Serial.println(numero);
  
  lcd.setCursor(0,0); 
  lcd.print("Num:");
  lcd.setCursor(5,0);
  lcd.print(numero);
  delay(2000);
  lcd.setCursor(0,1);
  //lcd.clear();
  lcd.print("                                         "); //Efface l'écran
  
  

    
  /*...ENVOIS...*/
  //message = "CECI EST UN TESTE DE MESSAGE";           // Save user input in the message variable
  /*
  LSMS.print("L'étudiant: "+numero);                    // Prepare message variable to be sent by LSMS
        
    if (LSMS.endSMS())                                 // If so, send the SMS
    {
      Serial.println("SMS sent");                       // Print "SMS sent" in serial port if sending is successful
    }
    else
    {
      Serial.println("SMS is not sent");              // Else print "SMS is not sent"
    } 
    */
}

/*
 * _________________________________________________________________________________________________________________
 * 
 *                ===================================Rendre la clée=======================================
 *                Ici on remet les valeur du début du programe afin de boucler la boucle 
 *                !!Probleme 
 * _________________________________________________________________________________________________________________
 *
 */

 void rendreClee(){
    buttonState = digitalRead(buttonPin);                 //Lire le bouton
    
    if(buttonState == HIGH){

      /*RESET LE NUMERO*/
      for(int i = 0 ; i <= 7; i++){                          
        numeroEtudiant[i] = 0;
      }

      saveState = 0;
      numero = "";
      isKeep = false;
      
      lcd.setCursor(0,0); 
      lcd.print("                                         "); //Efface l'écran
      
      lcd.print("_Clee rendu_"); 
      delay(700);
      lcd.print("                                         "); //Efface l'écran
      //lcd.clear();
    }
 }


 void StatutWifi() {
  //Affiche le nom de l'AP auquelle on c'est connecté 
  Serial.print("SSID: ");
  Serial.println(LWiFi.SSID());

  //Afficher le puissance du signal de l'AP
  long rssi = LWiFi.RSSI();
  Serial.print("signal strength (RSSI): ");
  Serial.print(rssi);
  Serial.println(" dBm");

  //Affiche l'adresse MAC
  if (strcmp(LWiFi.SSID(),"RT-WIFI-Guest")==0){
    uint8_t BSSID[VM_WLAN_WNDRV_MAC_ADDRESS_LEN] = {0};
    LWiFi.BSSID(BSSID);
    Serial.print("BSSID2 is: ");  
    sprintf(bssid2, "%02X:%02X:%02X:%02X:%02X:%02X", BSSID[0], BSSID[1], BSSID[2], BSSID[3], BSSID[4], BSSID[5]);   
    Serial.println(bssid2);
  }
}

//Localisation de la clé
void Localize(char *rtwifi[6][3]){ 
  int i;
  long rssi = LWiFi.RSSI();
  for (i=0;i<6;i++){
    if (strcmp(bssid2,rtwifi[i][0])== 0){
      Serial.println("---------------------------------");
      if(rssi < -58){ //La puissance réçus est inférieur au seuil définit (ici -58), on se trouve donc entre deux salles ayant un AP RT-Wifi
        Serial.print("Je suis avec l'étudiant "); 
        Serial.print(numero);
        Serial.print(" dans la salle ");
        Serial.print(rtwifi[i][2]); //On affiche le nom de cette salle
      }
      else{
        Serial.print("Je suis avec l'étudiant "); 
        Serial.print(numero);
        Serial.print(" dans la salle ");
       Serial.print(rtwifi[i][1]);//Affiche le nom de la salle
      }      
    }
  }
}


