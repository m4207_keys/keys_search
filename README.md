**Projet key_search !**

Nous sommes 2 dans ce projet:
Ludovic Fumabre 
Damien Etheve

Et nous avons repris le projet de Dylan

**BUT:** Le but de ce TP est de pouvoir gérer la gestion d'une clé dans le département RT, on devra 
-   Gérer l'emprunt (Important)
-   Gérer la localisation (Important)
-   Envoyer les donées
-   Avoir des indications sur la batterie

__________________________________________________

Le script pour localiser la clé se trouve dans le chemin code_src/Partie_Localisation/Localisation_Wifi. 
Le programme LocalisationSalle détermine le nom de la salle en fonction
des Point d'accès WiFi.
Afin de faire fonctionner le script, seul une antenne WiFi brancher sur le LinkitOne est nécessaire. 

__________________________________________________
Le script pour gérer l'emprunt se trouve dans code_src/Patie_Identification/final_emprunt2.
Ce programe gère les critères suivant

-   Entrer un numéro étudiant
-   Envois du numéro par GSM
-   Rendre la clée (Work In Progress)


Tout les détails concernant les branchement se trouve dans le code.

____________________________________________

**Gérer l'emprunt**

- But

a) Dans cette partie le but va être de savoir concretement, quels étudiants a emprunté la clé. Pour cela il va devoir s'identifier, grâce à un potentiometre il va séléctionner 
son numéro étudiant, puis le valider afin de le sauvegarder.

b) Par la suite, il faudra faire en sorte que l'étudiant rend la clé. Pour se faire quand l'étudiant aura fini, il activera un bouton permettant de dire que la clé a été rendue
et de tout remettre à 0 afin de boucler la boucle.


- Avancement

Reussite:   a
Echec:      b

**Gérer la localisation**

- But 

a) Dans cette partie nous allons être en mesure de savoir dans quelles salles la clée se trouve en utilisant le module WIFI. On comparera l'adresse MAC des AP avec les salles. 

        92:2A:A8:5A:5C:68","Administration"
        92:2A:A8:4A:AD:26","Proj-doc"
        92:2A:A8:4A:AD:2E","Reseaux cablages","Info Prog"
        92:2A:A8:4A:AF:2E","Signaux Systemes"
        92:2A:A8:47:CE:A5","TD1","TD2"
        92:2A:A8:4A:AC:55","TD3","Info Prog"
        
- Avancement

Reussite:   a
Echec:      null

**Envois des informations**

Pour finir il faut envoyer les informations sous forme de message SMS en utilisant la capacité GSM de la linkit one. Le SMS sera envoyé a l'Administration.